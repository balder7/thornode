package types

import common "gitlab.com/thorchain/thornode/common"

// PoolIndex just a slice of all the pools
type PoolIndex []common.Asset

package types

const (
	// module pooldata
	ModuleName = "thorchain"

	// StoreKey to be used when creating the KVStore
	StoreKey = ModuleName

	RouterKey = ModuleName // this was defined in your key.go file
)

package cmd

const Bech32PrefixAccAddr = "thor"
const Bech32PrefixAccPub = "thorpub"
const Bech32PrefixValAddr = "thorv"
const Bech32PrefixValPub = "thorvpub"
const Bech32PrefixConsAddr = "thorc"
const Bech32PrefixConsPub = "thorcpub"
